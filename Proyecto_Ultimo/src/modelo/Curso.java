package modelo;


import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public class Curso{
//----ATRIBUTOS----

    private String nombre;

    private Date fecha; //dia-mes-año

    private Date horario; //hora y minutos

    private final int max_alumnos = 20; //Damos por hecho por defecto máx 20

    private int alumnos_actuales;     //contador

    private Double precio;

    private String codigo_curso;

    private String cod_monitor; //= monitor.getCodigo;

//----CONSTRUCTOR----
    public Curso(String nombre, Date fecha, Date horario, Double precio, String codigo_curso, String cod_monitor) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.horario = horario;
        alumnos_actuales = 0;
// to do alumnos actuales ++ en usuarios cliente
        this.precio = precio;
        this.codigo_curso = codigo_curso;
        this.cod_monitor = cod_monitor;
        List <Cliente> clientela;
    }
//----METODOS----

    public void mandarCorreoConfirmacion(String correoCliente){
        try {
            //correoCliente lo obtenemos de BaseDatos
            //enviarCorreo cuando se escriban los datos en la base de datos
            String mensaje = "Estimado Cliente: <br> Le confirmamos su plaza en el curso solicitado <br> Para mas informacion comprueba las listas";

            Polideportivo.enviar(mensaje, correoCliente);
            
        } catch (MessagingException ex) {
            Logger.getLogger(Curso.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Curso.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void mandarCorreoPlazaLibre(String correoCliente) throws MessagingException, AddressException, ParseException {
        //obtenemos correoCliente de BD 
        //enviarCorreo cuando el cliente suba puesto en la lista de espera con plaza
        String mensaje = "Estimado Cliente: <br> Ha quedado una plaza libre en el curso solicitado";
        Polideportivo.enviar(mensaje, correoCliente);
    }

    public void mandarCorreoAviso2dias(List clientela) throws MessagingException, AddressException, ParseException {
        //obtenemos Lista Clientes de BD clientes en el curso
        //enviarCorreo a Todos los Cliente del curso avisando 2 días antes del comienzo del curso
        Iterator <Cliente> it = clientela.iterator();
        while (it.hasNext()){
            Cliente siguiente = it.next();
            String correoCliente = siguiente.geteMail();
            String mensaje = "Quedan 2 dias para empezar el curso";
            Polideportivo.enviar(mensaje, correoCliente);
        }
        
    }

    public void mandarCorreoModificacionCurso(List cliente) {
        //obtenemos Lista Clientes de BD clientes en el curso
        //enviarCorreo a todos los Clientes del curso correo avisando de las modificaciones
    }

}
