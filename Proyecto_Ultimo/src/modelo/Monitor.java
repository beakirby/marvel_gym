package modelo;


public class Monitor extends Usuario {
//----CONSTRUCTOR----

    public Monitor(Double sueldo, String nombre, String apellidos, String nif, String email, String telefono) {
        super(nombre, apellidos, nif, email, telefono);
        this.sueldo = sueldo;
    }
//----ATRIBUTOS----

    private Double sueldo;

//----METODOS----
    public void crearSuCurso() {
    }

    public void borrarCurso() {
    }

    public void bajaCurso() {
    }

    public void mostrarSusCursos() {
    }

    public void modificarDatos() {
    }
}
