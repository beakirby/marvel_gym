package modelo;


public class Administrador extends Usuario {
//----ATRIBUTOS----

    private Double sueldo;

//----CONSTRUCTOR----
    public Administrador(Double sueldo, String nombre, String apellidos, String nif, String email, String telefono) {
        super(nombre, apellidos, nif, email, telefono);
        this.sueldo = sueldo;

    }

//----METODOS----
    public void crearCurso() {
    }

    public void crearMonitor() {
    }

    public void bajaCurso() {
    }

    public void bajaMonitor() {
    }

    public void bajaCliente() {
    }

    public void modificarCurso() {
    }

    public void modificarMonitor() {
    }

    public void modificarDatos() {
    }
}
