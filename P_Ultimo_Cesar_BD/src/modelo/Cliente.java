package modelo;


import java.util.Date;

public abstract class Cliente extends Usuario {

//----ATRIBUTOS----
    private Date fecha_nacimiento;

    private String nombre_tutor;

    private Date fecha_alta;
//----CONSTRUCTOR----

    public Cliente(String nombre, String apellidos, String nif, String email, String telefono) {
        super(nombre, apellidos, nif, email, telefono);
    }
//----METODOS----

    public void modificarDatos() {
    }

    public void apuntarseCurso() {
    }

    public void borrarseCurso() {
    }

}
