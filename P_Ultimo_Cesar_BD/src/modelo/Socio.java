package modelo;


public class Socio extends Cliente {
//----ATRIBUTOS----

    private String cod_socio;

    private String modalidad;

    private Double cuota_mensual;

    private String cuenta_bancaria;

    private Double pago_cursos;

    private Double pago_total;
//----CONSTRUCTOR----

    public Socio(String cod_socio, String modalidad, Double cuota_mensual, String cuenta_bancaria, Double pago_cursos, Double pago_total, String nombre, String apellidos, String nif, String email, String telefono) {
        super(nombre, apellidos, nif, email, telefono);
        this.cod_socio = cod_socio;
        this.modalidad = modalidad;
        this.cuota_mensual = cuota_mensual;
        this.cuenta_bancaria = cuenta_bancaria;
        this.pago_cursos = pago_cursos;
        this.pago_total = pago_total;
    }

//----METODOS----
    public void modificarDatos() {
    }

    public void pagoTotal() {
    }
}
