package modelo;


import java.util.UUID;

public abstract class Usuario {
//----ATRIBUTOS----

    private String nombre;

    private String apellidos;

    private String nif;

    private String email;

    private String telefono;

    private String codigo_usuario;

    private String tipo_usuario;

    private final String PASSWORD_usuario;

//----CONSTRUCTOR----
    public Usuario(String nombre, String apellidos, String nif, String email, String telefono) {
        //pedir en Jdialog los valores y se los pasamos al constructor
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.nif = nif;
        this.email = email;
        this.telefono = telefono;
        this.codigo_usuario = asignarCodigo().substring(9, 13); //4 digitos 
        this.PASSWORD_usuario = asignarCodigo().substring(0, 7); //8 digitos
//hacer abstracta, porque no puede existir, es abstracta y cliente también
    }
//----METODOS----
    public void registrarse() {
        //pedir en Interfaz Gráfica los valores y se los pasamos al constructor
        //y lo guardamos en la base de datos
    }
    //public void modificar_datos() {
    ////TO DO Método para Interfaz Gráfica
    //    }

    public void recordar_codigo() {
        //Metodo interfaz grafica, boton solicitar codigo,
        //Solicitar codigo, comprobarCorreo existe en la Base de Datos
        //con codigo ya asignado, getcodigo_usuario + enviarCorreo

    }

    //public void logearse() {
    //    }
    public void mostrarCursos() {
    }

    public String asignarCodigo() {
    //genera un codigo aleatorio Y ÚNICO con 16 digitos, luego cogemos lo que necesitamos
        String nuevoCodigo = Polideportivo.generarCodeUUID();
        return nuevoCodigo;
    }

    public String getCodigo_usuario() {
        return codigo_usuario;
    }

    //-----SETTER MODIFICAR DATOS-----
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
